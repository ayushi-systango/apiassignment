import React from 'react';

import './SearchResult.css';

const SearchResult = (props) =>{
    const {posts, searchelement, checked} = props.state;
    console.log(props.state);
    
    const resultHandler = () =>{
        return(
            posts.map(post => {
                if(post[checked] == searchelement){
                    //console.log(comment.checked)
                    return(
                        <div key = {checked} >
                            <label><strong>Id </strong><p>  {post.id}    </p> </label>
                            <label><strong>Name</strong> <p>    {post.name}  </p></label>
                            <label><strong>Comment </strong><p> {post.body}  </p></label>
                        </div>

                    )
                }  
        })
    )}
    return(
        <div>
            <h3>Search result is :</h3>
                {resultHandler()}    
        </div>
    );
}

export default SearchResult;
