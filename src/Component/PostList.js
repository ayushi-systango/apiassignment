import React, { Component } from 'react';
import axios from 'axios';
import { JsonToTable } from "react-json-to-table";

import SearchBox from './SearchBox';
import SearchResult from './SearchResult';
import Modal from './Modal';


class PostList extends Component {
  constructor(props){
        super(props)
            this.state={
                posts:[],
                loading:false,
                searchelement:'',
                element:'id'
            
        }
    }
  
    componentDidMount() {
        this.setState({
            posts:[],
            loading:true
        })
    axios.get('https://jsonplaceholder.typicode.com/posts/1/comments')
        .then(response =>{
            console.log(response);
            this.setState({
                posts:response.data,
                loading:false
            })
        })
        .catch(error =>{
            console.log(error);
        })
    }

    handleInput=(e)=>
    {
        console.log(e.target.value);
       this.setState({searchelement:e.target.value})
    }

    render() {
        let filteredBlocks=this.state.posts.filter((post)=>{
           return post.email.toLowerCase().includes(this.state.searchelement.toLowerCase())

        });
        if(this.state.loading)
        {
            return(
                <p>Loading..!</p>
                );
        }
        const searchResult=<SearchResult state={this.state}/>
         return (
            <div className="App">
            <h3>Search Box</h3>
            <SearchBox handleInput={this.handleInput}/>
              <JsonToTable json={filteredBlocks} />
            
            <Modal post={this.state.post}>
            {searchResult}
            </Modal>
            </div>
            );
    }
}


export default PostList;
