import React, {Component} from 'react';

const SearchBox=(props)=>
{
	return(
		<div>
		<div>
		<label for="element"><h5>Select Element in Which you Want to Filter:</h5></label>
		<select id="element">
		<option value="email">Email</option>
		</select>
		</div>
		<div>
		<h4>Search Here</h4>
		<input onChange={props.handleInput} type="text" />
		</div>
		</div>
		);
}
export default SearchBox;